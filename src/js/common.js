$(function () {
    var header = $(".header");
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();

        if (scroll >= 100) {
            header.addClass("header_fix");
        } else {
            header.removeClass("header_fix");
        }
    });

    $('.main-menu__btn').on('click', function () {
        $(this).toggleClass('main-menu__btn_active')
        $('.main-menu__nav').slideToggle(300);
    });

    $('.carusel-slider').slick({
        centerMode: true,
        // centerPadding: '60px',
        slidesToShow: 1,
        variableWidth: true,
        responsive: [
            {
              breakpoint: 768,
              settings: {
                arrows: false,
                centerMode: true,
                variableWidth: false,
                centerPadding: '40px',
                slidesToShow: 1
              }
            }
          ]
    });


    $('.persons-slider').slick({
        dots: true,
        arrows: false,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true
    });
});