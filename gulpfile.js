// npm install
var gulp = require('gulp'),
    concat = require('gulp-concat'),
    cleanCSS = require('gulp-clean-css'),
    autoprefixer = require('gulp-autoprefixer'),
    scss = require("gulp-sass"),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    combineMq = require('gulp-combine-mq'),
    sourcemaps = require('gulp-sourcemaps'),
    notify = require("gulp-notify");

gulp.task('scripts', function() {
    return gulp.src(['src/js/libs.js','src/js/common.js'])
        .pipe(sourcemaps.init())
        .pipe(concat('main.js'))
        .pipe(uglify())
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('js'))
        .pipe(notify('Done JS'))

});

gulp.task('scss', function() {
    gulp.src("src/style/main.scss")
        .pipe(sourcemaps.init())
        .pipe(scss({ "bundleExec": false }).on("error", notify.onError()))
        .pipe(gulp.dest("src/style"))
        .pipe(autoprefixer({browsers: ['last 3 versions', '> 5%'], cascade: false}))
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('css'))
        .pipe(notify('Done CSS, Must Minify use task before upload to prod!'))
});

gulp.task('minify', function() {
    gulp.src("css/main.css")
        .pipe(combineMq({
            beautify: false
        }))
        .pipe(cleanCSS({
            compatibility: 'ie9',
        }))
        .pipe(gulp.dest('css'))
        .pipe(notify('Done Minify'))
});

gulp.task('img', function() {
    gulp.src('img/*.{png,jpg,gif}')
        .pipe(imagemin({
            optimizationLevel: 7,
            progressive: true
        }))
        .pipe(gulp.dest('img'))
        .pipe(notify('Done Images'));
});

gulp.task('watch', function () {
    gulp.watch(['css/**/*.scss','src/style/**/*.scss'], ['scss'] );
    gulp.watch(['src/js/*'], ['scripts'] );
    gulp.watch('img/*.{png,jpg,gif}', ['img']);
});

gulp.task('default', ['watch']);